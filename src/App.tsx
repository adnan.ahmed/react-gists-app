import React from "react";
import logo from "./logo.svg";
import "./App.css";
import { Layout } from "antd";
import { Content } from "antd/lib/layout/layout";
import { Headers } from "./components/Headers";
import {Home} from "./components/Home/Home"
function App() {
  return (
    <Layout>
      <Content>
        <Headers />
        <Home />
      </Content>
    </Layout>
  );
}

export default App;
