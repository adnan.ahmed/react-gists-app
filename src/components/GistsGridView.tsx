import { Card, Layout } from "antd";
import { Content } from "antd/lib/layout/layout";
export function GistsGridView() {
  return (
    <Layout>
      <Content>
        <Card
          title="Default size card"
          extra={<a href="#">More</a>}
          style={{ width: 300 }}
        >
          <p>Abc</p>
          <p>content</p>
          <p>xyz</p>
        </Card>
        <Card
          size="small"
          title="Small size card"
          extra={<a href="#">More</a>}
          style={{ width: 300 }}
        >
          <p>card 2</p>
          <p>Card 3</p>
          <p>Card 4</p>
        </Card>
      </Content>
    </Layout>
  );
}
