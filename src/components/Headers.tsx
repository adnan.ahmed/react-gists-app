import { Layout, Input, Button } from "antd";
import { Header } from "antd/lib/layout/layout";
import "./Headers.css";
export function Headers() {
  return (
    <>
      <Layout>
        <Header>
          <div className="logo"></div>
          <div className="searchInput">
            <Button>Login</Button>
          </div>
          <div className="searchInput">
            <Input placeholder="Search Gists"></Input>
          </div>
        </Header>
      </Layout>
    </>
  );
}
