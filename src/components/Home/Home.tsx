import { render } from "@testing-library/react";
import { Table,Switch, Grid } from "antd"
import { useEffect, useState } from "react";
import { GistsGridView } from "../GistsGridView";
import  ListView  from "../ListView";
import { GetList } from "../Services/Service";
export function Home(){
    const dataSource = [
        {
          key: '1',
          name: 'Mike',
          age: 32,
          address: '10 Downing Street',
        },
        {
          key: '2',
          name: 'John',
          age: 42,
          address: '10 Downing Street',
        },
      ];
      const columns = [
        {
          title: 'Name',
          dataIndex: 'name',
          key: 'name',
        },
        {
          title: 'Age',
          dataIndex: 'age',
          key: 'age',
        },
        {
          title: 'Address',
          dataIndex: 'address',
          key: 'address',
        },
      ];
    let [isListView,setListView] = useState(true);
    function changeView(checked:Boolean){
        console.log("is checked",checked)
        if(checked){
            setListView(false)
        }else{
            setListView(true)
        }
    }
    useEffect(()=>{
        console.log("react hook called");
        let data = GetList();
        console.log(data);
    },[])
   return (
       <>
       <Switch checkedChildren="Grid View"  unCheckedChildren="List View"   onChange={changeView} />
       {isListView && <ListView columns={columns} data={dataSource}/>}
       {!isListView && <GistsGridView />}
       </>
   )
}