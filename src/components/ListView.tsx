import { Table,Switch } from "antd"
import { configConsumerProps } from "antd/lib/config-provider";
import React from "react";
import { useState } from "react";
import { GistsGridView } from "./GistsGridView";
interface IProps{
    data:[],
    columns:[]
}
 function ListView(props:any){
    console.log(props);
    const {columns,data} = props;
    return (
        <>
          <Table dataSource={data} columns={columns} />
        </>
    )
}
export default React.memo(ListView)