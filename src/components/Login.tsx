import { render } from "@testing-library/react";
import { Layout } from "antd";
import { Form, Input, Button } from "antd";

export function Login() {
  render(
    <>
      <Form layout="inline" >
        <Form.Item>
            <Input
              placeholder="Username"
            />
        </Form.Item>
        <Form.Item>
          <Input type="password" placeholder="Password" />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">
            Log in
          </Button>
        </Form.Item>
      </Form>
    </>
  );
}
