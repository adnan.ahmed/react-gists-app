import axios from "axios";
export async function GetReq(url: string,params={}) {
  try {
    const config = {
      method: "get",
      url: `${url}`,
    };
    return await axios(config);
  } catch (error) {
    return error;
  }
}
export async function PostReq(url: string,body={}) {
    try {
      const config = {
        method: "post",
        url: `${url}`,
        data:body
      };
      return await axios(config);
    } catch (error) {
      return error;
    }
  }