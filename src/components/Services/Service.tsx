import { GetReq,PostReq } from "./Axios";
import { apiPath } from "../../Config";
export async function GetList(){
    try {
        let result = await GetReq(`${apiPath.getGistsList}`);
        return result;
    } catch (error) {
        throw error;
    }
}